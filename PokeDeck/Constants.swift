//
//  Constants.swift
//  PokeDeck
//
//  Created by Jon Roberts on 17/10/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation


let BASE_URL = "http://pokeapi.co"

let POKE_URL =  "/api/v1/pokemon/"



typealias DownloadComplete = () -> () 