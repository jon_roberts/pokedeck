//
//  Poke.swift
//  PokeDeck
//
//  Created by Jon Roberts on 16/10/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation
import Alamofire

class Poke {
    
    private var _name: String!
    private var _pokeId: Int!
    private var _desc: String!
    private var _type: String!
    private var _height: String!
    private var _weight: String!
    private var _defense: String!
    private var _attack: String!
    private var _nextEvoTxt: String!
    private var _nextEvoId: String!
    
    private var _pokemonURL: String!
    private var _nextEvoLvl: String!
    
    
    
    var name: String {
        
        
        return _name
        
        
    }
    
    var pokeId: Int {
        return _pokeId
       
    }
    
    var desc: String {
       
            if _desc == nil {
                _desc = ""
            }
        return _desc
       
        
    }
    
    var type: String{
        if _type == nil {
            _type = ""
        }
        return _type
    }
    var height: String {
        if _height == nil {
            _height = ""
        }
        return _height
    }
    var weight: String {
        if _weight == nil {
            _weight = ""
        }
        return _weight
    }
    var defense : String {
        if _defense == nil {
            _defense = ""
        }
        return _defense
    }
    var attack : String {
        if _attack == nil {
            _attack = ""
        }
        return _attack
    }
    var nextEvoTxt: String {
        if _nextEvoTxt == nil {
            _nextEvoLvl = ""
        }
        return _nextEvoTxt
    }
    var nextEvoId : String {
        if _nextEvoId == nil {
            
            _nextEvoId = ""
            
        }
    return _nextEvoId
    }
    var nextEvoLvl : String {
        if _nextEvoLvl == nil {
            _nextEvoLvl = ""
        }
    return _nextEvoLvl

    }
    
    
    
    init(name: String, id: Int){
        _name = name
        _pokeId = id
        _pokemonURL = "\(BASE_URL)\(POKE_URL)\(self._pokeId)"
        print(_pokemonURL)
        
    }
    
    func downloadPokeDetails(completed: DownloadComplete){
        
        let url = NSURL(string: _pokemonURL)!
        
        Alamofire.request(.GET, url).responseJSON { response in

            if let dict  = response.result.value as? Dictionary<String, AnyObject> {
            
                if let weight = dict["weight"] as? String {
                    self._weight = weight
                }
                if let height = dict["height"] as? String {
                    self._height = height
                }
                
                if let attack = dict["attack"] as? Int {
                    self._attack = "\(attack)"
                }
                if let defense = dict["defense"] as? Int {
                    self._defense  = "\(defense)"
                }
                
                if let types = dict["types"] as? [Dictionary<String, String>] where types.count > 0 {
                    if let name = types[0]["name"] {
                        self._type = name.capitalizedString
                    }
                    if types.count > 1 {
                        for var x = 1; x < types.count ; x++ {
                            if let name = types[x]["name"] {
                                self._type! += "/\(name.capitalizedString)"
                            }
                        }
                    }
                } else {
                    self._type = ""
                }
                
                if let descArr = dict["descriptions"] as? [Dictionary<String, String>] where descArr.count > 0 {
                    
                    if let url = descArr[0]["resource_uri"] {
                        
                        //  let nsurl = NSURL(string: "\(BASE_URL)\(url)")!
                        let nsurl = NSURL(string: "\(BASE_URL)\(url)")!
                        Alamofire.request(.GET, nsurl).responseJSON { descResponse in
                            
                            if let descDict = descResponse.result.value as? Dictionary<String, AnyObject> {
                                if let description = descDict["description"] as? String {
                                    self._desc = description
                                
                                }
                            }
                        completed()
                        }
                    }
                    
                    
                } else {
                    self._desc = " This is a boring pokemon!"
                }
                
                if let evos = dict["evolutions"] as? [Dictionary<String, AnyObject>] where evos.count > 0 {
                    if let to = evos[0]["name"] as? String {
                        // cant support mega atm but api still gives mega data
                        if to.rangeOfString("mega") == nil {
                            if let uri = evos[0]["resouces_uri"] as? String {
                                let newStr = uri.stringByReplacingOccurrencesOfString("/api/v1/pokemon/", withString: "")
                                let num = newStr.stringByReplacingOccurrencesOfString("/", withString: "")
                                
                                self._nextEvoId = num
                                self._nextEvoTxt = to
                                
                                if let lvl = evos[0]["level"] as? Int {
                                    self._nextEvoLvl = "\(lvl)"
                                } else {
                                    self._nextEvoLvl = ""
                                }
                            }
                            
                        }
                    }
                }
                
                
                
//                print(self._defense)
//
//                print(self._height)
//                                print(self._weight)
//                                print(self._attack)
//                print(self._type)
                //  print(self._desc)
            }
            // print(value?.debugDescription)
//            print(err.debugDescription)
//            
            }
        }
    
    
}