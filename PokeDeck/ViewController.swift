//
//  ViewController.swift
//  PokeDeck
//
//  Created by Jon Roberts on 16/10/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet var collection: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    
    var pokemon = [Poke]()
    var filteredPokemon = [Poke]()
    var musicPlayer: AVAudioPlayer!
    var searchMode = false
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collection.delegate = self
        collection.dataSource = self
        searchBar.delegate = self

        searchBar.returnKeyType = UIReturnKeyType.Done
        
        initAudio()
        parsePokemonCSV()
        
        
    }
    
    func initAudio(){
    
         let path = NSBundle.mainBundle().pathForResource("music", ofType: "mp3")!
        
        do {
            musicPlayer = try AVAudioPlayer(contentsOfURL: NSURL(string: path)!)
            musicPlayer.prepareToPlay()
            musicPlayer.numberOfLoops = -1
            musicPlayer.play()
            
        } catch let err as NSError {
            print (err.debugDescription)
        }
    
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" || searchBar.text == nil {
            searchMode = false
            view.endEditing(true)
            collection.reloadData()
        } else {
            searchMode = true
        }
        
        let lower = searchBar.text!.lowercaseString
        
        filteredPokemon = pokemon.filter({$0.name.rangeOfString(lower) != nil})
        
        collection.reloadData()
        
        
    }

    
    
    
    @IBAction func musicButtonPressed(sender: UIButton!) {
        
        if (musicPlayer.playing){
            musicPlayer.stop()
            sender.alpha = 0.2
        } else {
            musicPlayer.play()
            sender.alpha = 1.0
        }
        
    }
    
    func parsePokemonCSV(){
    
        let path = NSBundle.mainBundle().pathForResource("pokemon", ofType: "csv")
        
        do {
            let csv = try CSV (contentsOfURL: path!)
            let rows = csv.rows
            
            for row in rows {
                 let pokeId = Int(row["id"]!)!
                let name = row["identifier"]!
                
                let poke = Poke(name: name, id: pokeId)
                pokemon.append(poke)
            }
            
            
        } catch let err as NSError {
            print(err.debugDescription)
        }
        
    
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PokeCell", forIndexPath: indexPath ) as? PokeCell {
            
            var p :Poke!
            
            if searchMode {
                
                p = filteredPokemon[indexPath.row]
                
            } else {
                p = pokemon[indexPath.row]

            }
            
            
            
            cell.configureCell(p)
            return cell
        } else {
            return UICollectionViewCell()
        }
         
    }


    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
       
        
        var poke: Poke!
        if searchMode {
            poke = filteredPokemon[indexPath.row]
        } else {
            poke = pokemon[indexPath.row]
        }
        
        performSegueWithIdentifier("PokeDetailVC", sender: poke)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchMode {
            return filteredPokemon.count
        } else {
            return pokemon.count
        }
        

    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(105, 105)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PokeDetailVC" {
            if let detailsVC = segue.destinationViewController as? PokeDetailVC {
                if let p = sender as? Poke {
                    detailsVC.pokemon = p
                }
            }
        }
    }
    

}

