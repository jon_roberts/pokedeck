//
//  PokeCell.swift
//  PokeDeck
//
//  Created by Jon Roberts on 17/10/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {

    
    @IBOutlet var thumbImg: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    
    
    var pokemon: Poke!
    
    func configureCell(pokemon: Poke){
        self.pokemon = pokemon
        
        nameLbl.text = self.pokemon.name.capitalizedString
        thumbImg.image = UIImage(named:  "\(self.pokemon.pokeId)")
        
    
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 10.0
    }
    
}
