//
//  PokeDetailVC.swift
//  PokeDeck
//
//  Created by Jon Roberts on 17/10/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import UIKit

class PokeDetailVC: UIViewController {

    
    var pokemon: Poke!   
    
    //    @IBOutlet var testLbl: UILabel!
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var mainImg: UIImageView!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var typeLbl: UILabel!
    @IBOutlet var defLbl: UILabel!
    @IBOutlet var weightLbl: UILabel!
    @IBOutlet var heightLbl: UILabel!
    @IBOutlet var idLbl: UILabel!
    
    @IBOutlet var baseAttkLbl: UILabel!
    
    @IBOutlet var currentEvoImg: UIImageView!
    @IBOutlet var nextImgEvo: UIImageView!
    @IBOutlet var evoLbl: UILabel!
   
    @IBOutlet var theView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = pokemon.name
        let img = UIImage(named:  "\(pokemon.pokeId)")
        mainImg.image = img
        currentEvoImg.image = img
        pokemon.downloadPokeDetails { () -> () in
            // this will eb called when dl is done.
          self.updateUI()
        }
        
        
        
    }

    func updateUI(){
    
        // can use the values without unwrapping cause we set the getters to clean up nil values.
//        titleLbl.text = pokemon.name
//        mainImg.image = UIImage(named: "\(pokemon.pokeId)")
//
    
        descLbl.text = pokemon.desc
        typeLbl.text = pokemon.type
        defLbl.text = pokemon.defense
        heightLbl.text = pokemon.height
        weightLbl.text = pokemon.weight
        idLbl.text = "\(pokemon.pokeId)"
        baseAttkLbl.text = pokemon.attack
        
        
        if (pokemon.nextEvoId == "" ){
            evoLbl.text = "No Evoloutions"
            nextImgEvo.hidden = true
        } else {
             nextImgEvo.hidden = false
             nextImgEvo.image = UIImage(named: pokemon.nextEvoId)
            var str = "Next Evoloution: \(pokemon.nextEvoTxt)"
            if pokemon.nextEvoLvl != "" {
                str += " - LVL \(pokemon.nextEvoLvl)"
            }
        }
       
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        print("NOOOO:")
        dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
